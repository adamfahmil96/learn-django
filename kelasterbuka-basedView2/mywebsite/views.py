from django.views.generic.base import RedirectView, TemplateView

# Ini kok gak bisa diterapin ya seperti di tutorial?
class HomeView(RedirectView):
    pattern_name = 'index'

class HomeUserView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, *args, **kwargs):
        if self.request.GET.__contains__('tipe'):
            kwargs['tipe']  = self.request.GET['tipe']
        return super().get_context_data(*args, **kwargs)

# Inheritance hanya dari base view
class HomeRedirectView(RedirectView):
    pattern_name = 'user'
    permanent = False
    query_string = True
    def get_redirect_url(self, *args, **kwargs):
        print(kwargs)
        if kwargs['user'] == 'pukis':
            kwargs['user'] = 'adam'
        return super().get_redirect_url(*args, **kwargs)    # mengembalikan (return) pattern_name dan arguments (untuk menyerahkan data ke template)