from django import forms

class ContactForm(forms.Form):
    nama_lengkap    = forms.CharField(
        label='Nama Lengkap',
        max_length=20,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': 'Masukkan nama lengkap Anda',
            }
        ))
    GENDER = (
        ('P', 'Pria'),
        ('W', 'Wanita'),
    )
    jenis_kelamin   = forms.ChoiceField(
        widget=forms.RadioSelect(
            attrs={
                'class':'form-check-input'
            }
        ),
        choices=GENDER)
    TAHUN = range(1945, 2021, 1)
    tanggal_lahir   = forms.DateField(
        widget=forms.SelectDateWidget(
            attrs={
                'class':'form-control col-sm-2',
            },
            years=TAHUN))
    email           = forms.EmailField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isi dengan Email Anda',
            }
        ),
        label='Alamat Email')
    alamat          = forms.CharField(
        widget=forms.Textarea(
            attrs={
                'class':'form-control',
            }
        ),
        max_length=100,
        required=False)
    agree           = forms.BooleanField(
        widget=forms.CheckboxInput(
            attrs={
                'class':'form-check-input',
            }
        )
    )