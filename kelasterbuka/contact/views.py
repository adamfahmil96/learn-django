from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import ContactForm
from .models import ContactModel

# Create your views here.

def index(request):
    contact_form = ContactForm()
    context = {
        'Heading': 'Contact Form',
        'contact_form': contact_form,
    }
    print(request.POST)
    if request.method == 'POST':
        tgl_lahir = request.POST['tanggal_lahir_year']+"-"+request.POST['tanggal_lahir_month']+"-"+request.POST['tanggal_lahir_day']
        if request.POST['agree'] == 'on':
            bool_agree = True
        else:
            bool_agree = False
        ContactModel.objects.create(
            nama_lengkap    = request.POST['nama_lengkap'],
            jenis_kelamin   = request.POST['jenis_kelamin'],
            tanggal_lahir   = tgl_lahir,
            email           = request.POST['email'],
            alamat          = request.POST['alamat'],
            agree           = bool_agree,
        )
        return HttpResponseRedirect("/contact/lihat")
    return render(request, 'contact/index.html', context)

def lihat(request):
    contacts = ContactModel.objects.all()
    context = {
        'Heading': 'Lihat Contact',
        'Contacts': contacts
    }
    return render(request, 'contact/lihat.html', context)