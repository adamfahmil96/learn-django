from django.db import models

# Create your models here.
class ContactModel(models.Model):
    nama_lengkap = models.CharField(max_length=20)
    jenis_kelamin = models.CharField(max_length=10)
    tanggal_lahir = models.DateField()
    email = models.EmailField()
    alamat = models.CharField(max_length=100)
    agree = models.BooleanField()

    published = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{}. {}".format(self.id, self.nama_lengkap)