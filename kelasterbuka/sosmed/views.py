from django.shortcuts import render, redirect

from .models import Instagram
from .forms import InstagramForm

# Create your views here.
def index(request):
    akun    = Instagram.objects.all()
    context = {
        'page_title': 'Media Sosial',
        'akun_all': akun,
    }
    return render(request, 'sosmed/index.html', context)

def create(request):
    akun_form   = InstagramForm(request.POST or None)
    if request.method == 'POST':
        if akun_form.is_valid():
            akun_form.save()
        return redirect('sosmed:index')
    context = {
        'page_title': 'Tambah Akun',
        'akun_form': akun_form,
    }
    return render(request, 'sosmed/create.html', context)

def delete(request, delete_id):
    Instagram.objects.filter(id=delete_id).delete()
    return redirect('sosmed:index')

def update(request, update_id):
    akun_update = Instagram.objects.get(id=update_id)
    data = {
        'nama_depan': akun_update.nama_depan,
        'nama_belakang': akun_update.nama_belakang,
        'username': akun_update.username,
    }
    akun_form = InstagramForm(request.POST or None, initial=data, instance=akun_update)
    if request.method == 'POST':
        if akun_form.is_valid():
            akun_form.save()
        return redirect('sosmed:index')
    context = {
        'page_title': 'Ubah Akun',
        'akun_form': akun_form,
    }
    return render(request, 'sosmed/create.html', context)