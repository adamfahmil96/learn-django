from django.db import models

from .validators import validate_author

# Create your models here.
class Post(models.Model):
    judul       = models.CharField(max_length=100)
    body        = models.TextField()
    author      = models.CharField(
        max_length=100,
        validators= [validate_author]   # memasukkan validasi, jenis: list, tidak perlu makemigrations
    )

    LIST_CATEGORY = (
        ('Jurnal', 'jurnal'),
        ('Lagu', 'lagu'),
        ('Curhat', 'curhat'),
    )
    category    = models.CharField(
        max_length=100,
        choices=LIST_CATEGORY,
        default='Curhat',
    )

    def __str__(self):
        return "{}. {}".format(self.id, self.judul)