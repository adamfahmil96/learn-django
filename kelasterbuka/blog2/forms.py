from django import forms

from .models import Post

# menggunakan cara lama, mencocokkan langsung dengan models.py
'''
class PostForm(forms.Form):
    judul       = forms.CharField(max_length=100)
    body        = forms.CharField(widget=forms.Textarea)
    category    = forms.CharField(max_length=100)
'''

# menggunakan Model Form
class PostForm(forms.ModelForm):
    # mengakses dari inheritance class Model (models.py)
    class Meta:
        model = Post
        # tampilkan fields apa saja yang ingin diambil dari Model untuk form
        fields  = ['author', 'judul', 'body', 'category']
        # menambahkan atribut field (option aja)
        labels = {
            'author': 'Penulis'
        }
        widgets = {
            'judul': forms.TextInput(
                attrs   = {
                    'class': 'form-control',
                    'placeholder': 'Masukkan judul',
                }
            ),
            'author': forms.TextInput(
                attrs   = {
                    'class': 'form-control',
                    'placeholder': 'Masukkan nama penulis',
                }
            ),
            'body': forms.Textarea(
                attrs   = {
                    'class': 'form-control',
                }
            ),
            'category': forms.Select(
                attrs   = {
                    'class': 'form-control',
                }
            ),
        }