from django.shortcuts import render, redirect

# Create your views here.
from .forms import PostForm
from .models import Post

def index(request):
    posts = Post.objects.all()
    context = {
        'page_title': 'Post List',
        'posts': posts,
    }
    return render(request, 'blog2/index.html', context)

def create(request):
    # apabila tidak ada POST, maka dilakukan proses membuat object baru tanpa nilai isian (None)
    # apabila ada POST, maka dilakukan proses membuat object baru dengan nilai dari request POST tsb
    post_form = PostForm(request.POST or None)
    if request.method == 'POST':
        if post_form.is_valid():
            post_form.save()    # tidak perlu mengganti create seperti di bawah ketika ada perubahan
            '''
            Post.objects.create(
                judul       = request.POST.get('judul'),
                body        = request.POST.get('body'),
                category    = request.POST.get('category'),
            )
            '''
            return redirect('blog2:index')
    context = {
        'post_title': 'Create Post',
        'post_form': post_form,
    }
    return render(request, 'blog2/create.html', context)