from django.http import HttpResponse
from django.shortcuts import render

from .forms import FormField

# method
'''
def contact(request):

    judul = "<h1>CONTACT</h1>"
    subjudul = "<h2>Hubungi gue aja kalau berani!</h2>"

    output = judul + subjudul
    return HttpResponse(output)
'''

def about(request):
    return render(request, 'about.html')

def index(request):
    form_field = FormField()
    context = {
        'judul': 'Kelas Terbuka',
        'heading':'HOME',
        'kontributor': 'Hawa',
        'banner':'img/banner_home.png',
        'subheading': 'Using Django Web Framework',
        'nav':[
            ['/','Beranda'],
            ['/about','Tentang'],
            ['/contact','Hubungi Saya'],
            ['/blog','Blog'],
        ],
        'form_field': form_field,
    }

    if request.method == 'POST':
        print("Ini adalah method POST")
        context['nama'] = request.POST['nama']
        context['alamat'] = request.POST['alamat']
    else:
        print("Ini adalah method GET")
    return render(request, 'index.html', context)