from django.contrib import admin

# Register your models here.
from .models import Post

# mengubah segala sesuatu di bagian Admin
class PostAdmin(admin.ModelAdmin): 
    readonly_fields = ['slug',]

admin.site.register(Post, PostAdmin)