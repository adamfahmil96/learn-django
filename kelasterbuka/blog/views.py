from django.shortcuts import render
from django.http import HttpResponse

from .models import Post
from .forms import PostForm

# Create your views here.
def index(request):
    # query set
    posts = Post.objects.all()  # mengambil semua
    
    post_form = PostForm(request.POST or None)
    error = None
    if request.method == 'POST':
        if post_form.is_valid():
            Post.objects.create(
                title       = post_form.cleaned_data.get('title'),
                body        = post_form.cleaned_data.get('body'),
                category    = post_form.cleaned_data.get('category'),
                email       = post_form.cleaned_data.get('email'),
                alamat      = post_form.cleaned_data.get('alamat'),
            )
        else:
            error = post_form.errors

    context = {
        'judul':'Blog Kelas Terbuka',
        'kontributor':'Lima Persen Official',
        'subheading':'Blog using Django Web Framework',
        'banner': 'blog/img/banner_blog.png',
        # 'nav':[
        #     ['/','Beranda'],
        #     ['/blog/story','Cerita'],
        #     ['/blog/news','News'],
        # ]
        'nav':[
            ['/','Beranda'],
            ['/about','Tentang'],
            ['/contact','Hubungi Saya'],
            ['/blog','Blog'],
        ],
        'app_css': 'blog/css/styles.css',
        'posts': posts,
        'post_form': post_form,
        'error': error,
    }
    return render(request, 'blog/index.html', context)

def recent(request):
    return HttpResponse('<h1>Ini adalah recent post</h1>')

def news(request):
    context = {
        'judul':'News',
        'kontributor':'Adam',
    }
    return render(request, 'blog/index.html', context)

def story(request):
    context = {
        'judul':'Story',
        'kontributor':'Herlina',
    }
    return render(request, 'blog/index.html', context)

def noncategory(request):
    # query set
    posts = Post.objects.filter(category='Non Category')  # mengambil semua dengan filter tsb
    
    context = {
        'judul':'Blog Kelas Terbuka',
        'kontributor':'Kelas Terbuka',
        'subheading':'Blog using Django Web Framework',
        'banner': 'blog/img/banner_blog.png',
        'nav':[
            ['/','Beranda'],
            ['/about','Tentang'],
            ['/contact','Hubungi Saya'],
            ['/blog','Blog'],
        ],
        'app_css': 'blog/css/styles.css',
        'posts': posts
    }
    return render(request, 'blog/index.html', context)

def link(request, page):
    str = "<h1>{}</h1>".format(page)
    return HttpResponse(str)

def angka(request, input):
    heading = "<h1>Page Angka</h1>"
    str = heading + input
    return HttpResponse(str)

def tanggal(request, **input):
    print(input)
    tahun = input['tahun']
    bulan = input['bulan']
    hari = input['hari']
    heading = "<h1>Page Tanggal</h1>"
    dataTanggal = "<h2>{}/{}/{}</h2>".format(tahun,bulan,hari)
    return HttpResponse(heading + dataTanggal)

'''
def tanggal(request, tahun, bulan, hari):
    heading = "<h1>Page Tanggal</h1>"
    strTahun = "tahun: " + tahun
    strBulan = "bulan: " + bulan
    strHari = "hari: " + hari
    str = heading + strTahun + "<br>" + strBulan + "<br>" + strHari
    return HttpResponse(str)
'''

def categoryPost(request, categoryInput):
    posts = Post.objects.filter(category=categoryInput)
    return HttpResponse("Category Post")

def singlePost(request, slugInput):
    posts = Post.objects.get(slug=slugInput)
    title = "<h1>{}</h1>".format(posts.title)
    body = "<p>{}</p>".format(posts.body)
    category = "<p>{}</p>".format(posts.category)

    return HttpResponse(title + body + category)