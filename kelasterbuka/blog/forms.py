from django import forms

class PostForm(forms.Form):
    title = forms.CharField(max_length=255)
    body = forms.CharField(
        widget=forms.Textarea
    )
    category = forms.CharField(max_length=200)
    email = forms.CharField(
        widget=forms.EmailInput
    )
    alamat = forms.CharField(max_length=200)

    # menambahkan aturan baru untuk clean data (form validation)

    def clean_title(self):
        title_input = self.cleaned_data.get('title')
        if title_input == "harry":
            raise forms.ValidationError("Itu adalah kata yang tidak boleh disebutkan!")
        return title_input