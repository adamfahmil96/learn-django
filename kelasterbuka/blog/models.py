from django.db import models
from django.utils.text import slugify

# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=255)
    body = models.TextField()
    category = models.CharField(max_length=200, default='Non Category')
    email = models.EmailField(default='nama@web.com')
    alamat = models.CharField(max_length=200, blank=True)
    waktu_posting = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField(blank=True, editable=False)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Post, self).save()   # ambil super dari class Post, juga self, lalu panggil save()

    def __str__(self):
        return "{}".format(self.title)
    