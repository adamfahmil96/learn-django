from django.urls import path, re_path

from . import views

urlpatterns = [
    path('recent/', views.recent, name="recent"),
    path('story/', views.story, name="story"),
    path('news/', views.news, name="news"),
    path('non-category/', views.noncategory, name="noncategory"),
    path('', views.index, name="index"),
    re_path(r'^(?P<input>[0-9]{2})/$', views.angka, name="angka"),
    re_path(r'^(?P<tahun>[0-9]{4})/(?P<bulan>[0-9]{2})/(?P<hari>[0-9]{2})/$', views.tanggal, name="tanggal"),
    re_path(r'^(?P<page>[\w_]+)/$', views.link, name="link"),
    re_path(r'^(?P<categoryInput>[\w-]+)/$', views.categoryPost, name="categoryPost"),
    re_path(r'^post/(?P<slugInput>[\w-]+)/$', views.singlePost, name="singlePost"),
]