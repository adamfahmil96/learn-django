from django.shortcuts import render

# Create your views here.
def index(request):
    context = {
        'judul': 'Tentang Kelas Terbuka',
        'kontributor':'Herlina',
        'banner': 'about/img/banner_about.png',
        'subheading':'About page using Django Web Framework',
        'nav': [
            ['/','Beranda'],
            ['/about','Tentang'],
            ['/contact','Hubungi Saya'],
            ['/blog','Blog'],
        ]
    }
    return render(request, 'about/index.html', context)