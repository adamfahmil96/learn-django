from django.shortcuts import render
from django.http import HttpResponse
from .models import Dosen

# Create your views here.
def index(request):
    dosen = Dosen.objects.all()
    context = {
        "dosen": dosen
    }
    return render(request, 'siakad/index.html', context)