from django.apps import AppConfig


class SiakadConfig(AppConfig):
    name = 'siakad'
