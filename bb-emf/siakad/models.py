from django.db import models

# Create your models here.
class Dosen(models.Model):
    nik = models.CharField(primary_key=True, blank=False, max_length=20)
    nama = models.CharField(max_length=100)
    jabatan = models.CharField(max_length=100)
    alamat = models.CharField(max_length=100)

    def __str__(self):
        return self.nik + ":" + self.nama

class Mahasiswa(models.Model):
    nim = models.CharField(primary_key=True, blank=False, max_length=10)
    nama = models.CharField(max_length=100)
    tanggal_lahir = models.DateField()
    alamat = models.CharField(max_length=200)
    angkatan = models.CharField(max_length=4)
    ipk = models.FloatField(max_length=10)
    
    def __str__(self):
        return self.nim + ":" + self.nama
    