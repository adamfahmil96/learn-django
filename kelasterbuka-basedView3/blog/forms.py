from django import forms

# import model
from .models import Artikel

# class
class ArtikelForm(forms.ModelForm):
    class Meta:
        model   = Artikel
        fields  = ['judul', 'isi', 'penulis']