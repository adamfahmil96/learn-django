from django.urls import path, include, re_path
from django.views.generic import ListView, DetailView, FormView

from .views import ArtikelListView, ArtikelDetailView, ArtikelFormView
from .models import Artikel
from .forms import ArtikelForm

urlpatterns = [
    path('create/', ArtikelFormView.as_view(), name="create"),
    # path('create/', FormView.as_view(form_class=ArtikelForm, template_name="blog/artikel_create.html"), name="create"),
    # re_path(r'^detail/(?P<slug>[\w-]+)/$', DetailView.as_view(model=Artikel), name="detail"),
    re_path(r'^detail/(?P<slug>[\w-]+)/$', ArtikelDetailView.as_view(), name="detail"),
    re_path(r'(?P<penulis>[\w]+)/(?P<page>[\d]+)/$', ArtikelListView.as_view(), name="list"),
    re_path(r'(?P<penulis>[\w]+)/$', ArtikelListView.as_view(), name="list"),
    # re_path(r'(?P<page>[\d]+)/$', ArtikelListView.as_view(), name="list"),
    # path('', ArtikelListView.as_view(), name="list"),
    # path('', ListView.as_view(model=Artikel), name="list"),
]