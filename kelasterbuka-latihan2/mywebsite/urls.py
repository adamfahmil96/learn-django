from django.urls import path, re_path, include
from django.contrib import admin

urlpatterns = [
	path('sosmed/', include(('sosmed.urls', 'sosmed'), namespace='sosmed')),
    path('admin/', admin.site.urls),
]
