from django.contrib import admin
from django.urls import path, re_path
from django.views.generic.base import TemplateView

from .views import index, IndexClassView, IndexTemplateView, ContextTemplateView, ParameterTemplateView

urlpatterns = [
    path('', index, name="home"),
    path('class', IndexClassView.as_view(template_name='index.html'), name="home-class"),
    path('class2', IndexClassView.as_view(template_name='index2.html'), name="home-class2"),
    path('template', IndexTemplateView.as_view(template_name='index3.html'), name="template"),
    path('template-default', TemplateView.as_view(template_name='default.html'), name="template-default"), # for static template
    path('context', ContextTemplateView.as_view(), name="context"),
    re_path(r'parameter/(?P<parameter1>[0-9]+)/(?P<parameter2>[0-9]+)/$', ParameterTemplateView.as_view(), name="parameter"),
    path('admin/', admin.site.urls),
]

'''
CATATAN untuk Tutorial TemplateView
1. Membuat Class View di views.py, tapi menggunakan templatenya URL
2. Jika halaman kita itu statis (tidak ada perubahan apapun), maka kita lakukan TemplateView langsung pada urls.py
3. Membuat Views dengan Context saja, kita menggunakan Class TemplateView di views.py
4. Kita memasukkan parameter ke dalam template, dengan menggunakan regex (Django >2 misal pakai int:parameter)
'''