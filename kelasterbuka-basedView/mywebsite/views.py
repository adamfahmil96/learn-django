from django.shortcuts import render
from django.views import View
from django.views.generic.base import TemplateView

def index(request):
    context = {
        'heading': 'SELAMAT DATANG',
    }
    if request.method == 'POST':
        context['heading'] = 'POST function based view'
    return render(request,'index.html', context)

class IndexClassView(View):

    # menggunakan atribut dari parent class View
    template_name   = ''
    context = {}

    # override method GET dari parent class View
    def get(self, request):
        self.context['heading'] = 'GET class based view'
        return render(request, self.template_name, self.context)
    
    # override method POST dari parent class View
    def post(self, request):
        self.context['heading'] = 'POST class based view'
        return render(request, self.template_name, self.context)

# inheritance dari TemplateResponseMixin, ContextMixin, dan View
class IndexTemplateView(TemplateView):
    pass

class ContextTemplateView(TemplateView):
    template_name   = 'context.html'

    def get_context_data(self):
        context = {
            'judul': 'Home Context',
            'penulis': 'Herlina',
        }
        return context

class ParameterTemplateView(TemplateView):
    template_name   = 'parameter.html'

    def get_context_data(self, *args, **kwargs):
        # context = kwargs  ## cara pertama
        context = super().get_context_data(**kwargs)
        context['judul'] = 'HOME PARAMETER'
        context['penulis'] = 'Arjianti'
        return context