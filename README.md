# Learn Django Web Framework

The project for learning Django Web Framework. Created by: *Muhammad Adam Fahmil 'Ilmi*

## Git Command Line

**Git global setup**
```
git config --global user.name "YOUR NAME"
git config --global user.email "YOUR EMAIL"
```

**Create a new repository**
```
git clone https://gitlab.com/YOUR_USERNAME/learn-django.git
cd learn-django
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

**Push an existing folder**
```
cd EXISTING_FOLDER
git init
git remote add origin https://gitlab.com/YOUR_USERNAME/learn-django.git
git add . (OR git add EXISTING_FOLDER/FILE)
git commit -m "Initial commit"
git push -u origin master
```

**Push an existing Git repository**
```
cd EXISTING_FOLDER
git remote rename origin old-origin
git remote add origin https://gitlab.com/YOUR_USERNAME/learn-django.git
git push -u origin -all
git pusth -u origin --tags
```

**Check updated files/folders**
```
git status
```