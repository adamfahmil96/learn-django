from django.db import models
from django.conf import settings
from django.utils.text import slugify
from django.db.models import Q

# Create your models here.
class BlogQuerySet(models.QuerySet):
    def search(self, query=None):
        qs  = self
        if (query is not None):
            or_lookup   = (Q(title__icontains=query) | Q(description__icontains=query) | Q(slug__icontains=query))
            qs          = qs.filter(or_lookup).distinct()
        return qs


class BlogManager(models.Manager):
    def get_queryset(self):
        return BlogQuerySet(self.model, using=self._db)
    
    # mendevelop method search sendiri
    def search(self, query=None):
        return self.get_queryset().search(query=query)

class Post(models.Model):
    user            = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title           = models.CharField(max_length=120)
    description     = models.TextField(null=True, blank=True)
    slug            = models.SlugField(blank=True, unique=True)
    publish_date    = models.DateTimeField(auto_now_add=False, auto_now=False, null=True, blank=True)
    timestamp       = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{}. {} - {}".format(self.id, self.user, self.title)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Post, self).save()
    
    objects = BlogManager() # menyesuaikan model manager itu sendiri