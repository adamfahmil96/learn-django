from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path('', include(('search.urls', 'search'), namespace="index")),
    path('admin/', admin.site.urls),
]
