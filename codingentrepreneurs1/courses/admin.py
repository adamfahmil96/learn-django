from django.contrib import admin

# Register your models here.
from .models import Lesson

class LessonAdmin(admin.ModelAdmin): 
    readonly_fields = ['slug',]

admin.site.register(Lesson, LessonAdmin)