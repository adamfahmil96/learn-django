from itertools import chain     # package yang mengizinkan kita untuk mengikat querysets lalu mengubahnya menjadi instance queryset baru berbentuk list
from django.shortcuts import render
from django.views.generic import ListView

# Models
from blog.models import Post
from courses.models import Lesson
from profiles.models import Profile

# Create your views here.
class SearchView(ListView):
    template_name   = 'search/view.html'
    paginate_by     = 20
    count           = 0

    def get_context_data(self, *args, **kwargs):
        context     = super().get_context_data(*args, **kwargs)
        context['count']    = self.count or 0
        context['query']    = self.request.GET.get('cari')
        return context
    
    def get_queryset(self):
        request     = self.request
        query       = request.GET.get('cari', None)
        if query is not None:
            blog_results        = Post.objects.search(query)
            lesson_results      = Lesson.objects.search(query)
            profile_results     = Profile.objects.search(query)
            
            # combine querysets 
            queryset_chain = chain(
                    blog_results,
                    lesson_results,
                    profile_results
            )        
            qs = sorted(queryset_chain, 
                        key=lambda instance: instance.pk, 
                        reverse=True)
            self.count = len(qs) # since qs is actually a list
            return qs
        return Post.objects.none()