from django.urls import path, re_path

from .views import SearchView

urlpatterns = [
    path('', SearchView.as_view(), name="index"),
]